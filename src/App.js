import { Row, Col, Card, Typography, Spin } from "antd";
import "./App.css";
import { useRequest, useInterval } from "ahooks";
import axios from "axios";

const { Text } = Typography;

const filter = {
  auctionType: "Sale",
  criteria: {
    classes: [],
    parts: [],
    hp: null,
    speed: null,
    skill: null,
    morale: null,
    breedCount: null,
    pureness: [],
    numMystic: [],
    title: null,
    region: null,
    stages: [3, 4],
  },
  from: 0,
  size: 24,
  sort: "PriceAsc",
  owner: null,
};

const queryText = `
query GetAxieBriefList($auctionType: AuctionType, $criteria: AxieSearchCriteria, $from: Int, $sort: SortBy, $size: Int, $owner: String) {
  axies(auctionType: $auctionType, criteria: $criteria, from: $from, sort: $sort, size: $size, owner: $owner) {
    total
    results {
      ...AxieBrief
      __typename
    }
    __typename
  }
}

fragment AxieBrief on Axie {
  id
  name
  stage
  class
  breedCount
  image
  title
  genes
  battleInfo {
    banned
    __typename
  }
  auction {
    currentPrice
    currentPriceUSD
    __typename
  }
  stats {
    ...AxieStats
    __typename
  }
  parts {
    id
    name
    class
    type
    specialGenes
    __typename
  }
  __typename
}

fragment AxieStats on AxieStats {
  hp
  speed
  skill
  morale
  __typename
}
`;

const axiosGetData = () => {
  return axios.post("https://axieinfinity.com/graphql-server-v2/graphql", {
    query: queryText,
    variables: JSON.stringify(filter),
  });
};

const Description = ({ desc, content }) => (
  <p>
    <Text type="secondary">{desc}: </Text>
    <strong>{content}</strong>
  </p>
);

const openInNewTab = (url) => {
  const newWindow = window.open(url, "_blank", "noopener,noreferrer");
  if (newWindow) newWindow.opener = null;
};

function App() {
  const { data, loading, run } = useRequest(axiosGetData);

  useInterval(() => {
    run();
  }, 10 * 1000);

  console.log(loading, data);

  return (
    <div className="App">
      {data && (
        <Spin spinning={loading} tip={"正在加载数据……"}>
          <p>共搜索到 {data?.data?.data?.axies.total} 条数据</p>
          <Row gutter={[16, 16]} style={{ alignItems: "stretch" }}>
            {data?.data?.data?.axies?.results.map((axie) => (
              <Col key={axie.id} span={4}>
                <Card
                  style={{ height: "100%" }}
                  hoverable
                  cover={<img src={axie.image} alt={axie.name} />}
                  onClick={() =>
                    openInNewTab(
                      `https://marketplace.axieinfinity.com/axie/${axie.id}`
                    )
                  }
                >
                  <Description desc={"Name"} content={axie.name} />
                  <Description desc={"Class"} content={axie.class} />
                  <Description desc={"Breed Count"} content={axie.breedCount} />
                  <Description
                    desc={"Price"}
                    content={`${Number.parseFloat(
                      axie.auction.currentPrice / 10 ** 18
                    ).toFixed(5)} (~$${axie.auction.currentPriceUSD})`}
                  />
                </Card>
              </Col>
            ))}
          </Row>
        </Spin>
      )}
    </div>
  );
}

export default App;
